package com.erika.carshare.helpers

import android.app.Application

class App : Application() {
    companion object {
        var isDriving: Boolean = false
    }

    override fun onCreate() {
        super.onCreate()
        isDriving = false
    }
}