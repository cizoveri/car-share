package com.erika.carshare.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.erika.carshare.R
import com.erika.carshare.helpers.App
import kotlinx.android.synthetic.main.fragment_item_one.*

class ItemOneFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drive_image.setOnClickListener {
            if (App.isDriving) {
                drive_image.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_drive_stop))
                App.isDriving = false
            } else {
                drive_image.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_drive))
                App.isDriving = true
            }
        }
    }

    companion object {
        fun newInstance(): ItemOneFragment {
            return ItemOneFragment()
        }
    }
}
